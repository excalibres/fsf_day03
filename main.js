/**
 * Created by jk_87 on 30/3/2016.
 */
//load express after you done npm save --express
var express = require("express");
//create an application
var app = express();

//Use this directory as the document root --> "public"
app.use ( express.static(__dirname + "/public") );

// Start our web server on port 3000
app.listen(3000, function () {
    console.info("My web server has started on port 3000");
    console.info("Document root is at " +__dirname + "/public");
});
